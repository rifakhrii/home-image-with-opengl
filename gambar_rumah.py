from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

def init():
    glClearColor(0.0, 0.0, 0.0, 0.0) 
    gluOrtho2D(-50.0, 50.0, -50.0, 50.0)

def ploting():
    glClear(GL_COLOR_BUFFER_BIT)
    #Atas dari Jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-2.0, 23.0)
    glVertex2f(2.0, 23.0)
    glVertex2f(2.0, 24.0)
    glVertex2f(-2.0, 24.0)
    glVertex2f(-2.0,23.0)
    glEnd()
    glFlush()
    #Atas Jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-3.0, 22.0)
    glVertex2f(3.0, 22.0)
    glVertex2f(3.0, 23.0)
    glVertex2f(-3.0, 23.0)
    glVertex2f(-3.0,22.0)
    glEnd()
    glFlush()
    #Jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-2.0, 22.0)
    glVertex2f(2.0, 22.0)
    glVertex2f(2.0, 16.0)
    glVertex2f(-2.0, 16.0)
    glVertex2f(-2.0,22.0)
    glEnd()
    glFlush()
    #kotak dalam jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-2.0, 19.0)
    glVertex2f(-1.0, 19.0)
    glVertex2f(-1.0, 16.0)
    glVertex2f(-2.0, 16.0)
    glVertex2f(-2.0, 19.0)
    glEnd()
    glFlush()
    #kotak dalam jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-2.0, 22.0)
    glVertex2f(-1.0, 22.0)
    glVertex2f(-1.0, 19.0)
    glVertex2f(-2.0, 19.0)
    glVertex2f(-2.0, 22.0)
    glEnd()
    glFlush()
    #kotak dalam jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(2.0, 19.0)
    glVertex2f(1.0, 19.0)
    glVertex2f(1.0, 16.0)
    glVertex2f(2.0, 16.0)
    glVertex2f(2.0, 19.0)
    glEnd()
    glFlush()
    #kotak dalam jendela
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(2.0, 22.0)
    glVertex2f(1.0, 22.0)
    glVertex2f(1.0, 19.0)
    glVertex2f(2.0, 19.0)
    glVertex2f(2.0, 22.0)
    glEnd()
    glFlush()
    
    #pintu
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-4.0, 4.0)
    glVertex2f(4.0, 4.0)
    glVertex2f(4.0, -4.0)
    glVertex2f(-4.0, -4.0)
    glVertex2f(-4.0, 4.0)
    glEnd()
    glFlush()

    #atas pintu
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-4.0, 6.0)
    glVertex2f(-4.0, 8.0)
    glVertex2f(4.0, 8.0)
    glVertex2f(4.0, 6.0)
    glVertex2f(-4.0,6.0)
    glEnd()
    glFlush()
    
    #tengah pintu
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-5.0, 6.0)
    glVertex2f(5.0, 6.0)
    glVertex2f(5.0, 4.0)
    glVertex2f(-5.0, 4.0)
    glVertex2f(-5.0,6.0)
    glEnd()
    glFlush()

    #kotak jendela kanan
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(12.0, 8.0)
    glVertex2f(18.0, 8.0)
    glVertex2f(18.0, 0.0)
    glVertex2f(12.0, 0.0)
    glVertex2f(12.0,8.0)
    glEnd()
    glFlush()

    #kotak jendela kiri
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-12.0, 8.0)
    glVertex2f(-18.0, 8.0)
    glVertex2f(-18.0, 0.0)
    glVertex2f(-12.0, 0.0)
    glVertex2f(-12.0,8.0)
    glEnd()
    glFlush()

    #isi jendela kanan 1
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(12.0, 8.0)
    glVertex2f(14.0, 8.0)
    glVertex2f(14.0, 4.0)
    glVertex2f(12.0, 4.0)
    glVertex2f(12.0,8.0)
    glEnd()
    glFlush()

    #isi jendela kanan 2
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(12.0, 0.0)
    glVertex2f(14.0, 0.0)
    glVertex2f(14.0, 4.0)
    glVertex2f(12.0, 4.0)
    glVertex2f(12.0,0.0)
    glEnd()
    glFlush()

    #isi jendela 3
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(16.0, 4.0)
    glVertex2f(18.0, 4.0)
    glVertex2f(18.0, 0.0)
    glVertex2f(16.0, 0.0)
    glVertex2f(16.0, 4.0)
    glEnd()
    glFlush()

    #isi jendela kanan 4
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(16.0, 8.0)
    glVertex2f(18.0, 8.0)
    glVertex2f(18.0, 4.0)
    glVertex2f(16.0, 4.0)
    glVertex2f(16.0,8.0)
    glEnd()
    glFlush()


    #isi jendela kiri 1
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-12.0, 8.0)
    glVertex2f(-14.0, 8.0)
    glVertex2f(-14.0, 4.0)
    glVertex2f(-12.0, 4.0)
    glVertex2f(-12.0, 8.0)
    glEnd()
    glFlush()

    #isi jendela kiri 2
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-12.0, 0.0)
    glVertex2f(-14.0, 0.0)
    glVertex2f(-14.0, 4.0)
    glVertex2f(-12.0, 4.0)
    glVertex2f(-12.0, 0.0)
    glEnd()
    glFlush()

    #isi jendela kiri 3
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-16.0, 4.0)
    glVertex2f(-18.0, 4.0)
    glVertex2f(-18.0, 0.0)
    glVertex2f(-16.0, 0.0)
    glVertex2f(-16.0, 4.0)
    glEnd()
    glFlush()

    #isi jendela kiri 4
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-16.0, 8.0)
    glVertex2f(-18.0, 8.0)
    glVertex2f(-18.0, 4.0)
    glVertex2f(-16.0, 4.0)
    glVertex2f(-16.0, 8.0)
    glEnd()
    glFlush()

    #tembok
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-20.0, 14.0)
    glVertex2f(20.0, 14.0)
    glVertex2f(20.0, -4.0)
    glVertex2f(-20.0, -4.0)
    glVertex2f(-20.0, 14.0)
    glEnd()
    glFlush()

    #atap
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-16, 14.0)
    glVertex2f(-24, 14.0)
    glVertex2f(-22.0, 24.0)
    glVertex2f(-6.0, 24.0)
    glVertex2f(-16.0, 14.0)
    glVertex2f(16.0, 14.0)
    glVertex2f(6.0, 24.0)
    glVertex2f(22.0, 24.0)
    glVertex2f(24.0, 14.0)
    glVertex2f(16.0, 14.0)
    glEnd()
    glFlush()

    #segitiga atap
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(6.0, 24.0)
    glVertex2f(0.0, 30.0)
    glVertex2f(-6.0, 24.0)
    glEnd()
    glFlush()    

    #pondasi
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(-24, -4.0)
    glVertex2f(24.0, -4.0)
    glVertex2f(24.0, -10.0)
    glVertex2f(-24.0, -10.0)
    glVertex2f(-24.0, -4.0)
    glEnd()
    glFlush()

    #tangga 1   
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(8.0, -4.0)
    glVertex2f(8.0, -6.0)
    glVertex2f(-8.0, -6.0)
    glVertex2f(-8.0, -4.0)
    glVertex2f(8.0,-4.0)
    glEnd()
    glFlush()

    #tangga 2
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(10.0, -6.0)
    glVertex2f(10.0, -8.0)
    glVertex2f(-10.0, -8.0)
    glVertex2f(-10.0, -6.0)
    glVertex2f(10.0, -6.0)
    glEnd()
    glFlush()

    #tangga 3
    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINE_STRIP)
    glVertex2f(12.0, -8.0)
    glVertex2f(12.0, -10.0)
    glVertex2f(-12.0, -10.0)
    glVertex2f(-12.0, -8.0)
    glVertex2f(12.0, -8.0)
    glEnd()
    glFlush()


    
def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB)
    glutInitWindowSize(800,800)
    glutInitWindowPosition(100,100)
    glutCreateWindow("Gambar Rumah")
    glutDisplayFunc(ploting)
    

    init()
    glutMainLoop()
main()
